import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { ListComponent } from './list/list.component';
import { ListitemComponent } from './listitem/listitem.component';
import { HelpComponent } from './help/help.component';
import { Routes, RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    ListComponent,
    ListitemComponent,
   HelpComponent
  ],
  imports: [
    BrowserModule,
      RouterModule.forRoot([
          {path:'',component:MainComponent},
          {path:'List',component:ListComponent},
          {path:'Listitem',component:ListitemComponent},
          {path:'Help',component:HelpComponent},
          {path:'**',component:MainComponent}
        ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
