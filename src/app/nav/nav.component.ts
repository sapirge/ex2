import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

   toHelp(){
    
        this.router.navigate(['/Help']);
      }
    
      toList(){
    
        this.router.navigate(['/List']);
     }

    constructor(private router:Router) { }


  ngOnInit() {
  }

}
