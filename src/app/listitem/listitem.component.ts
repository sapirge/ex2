import { Component, OnInit , Input } from '@angular/core';

@Component({
  selector: 'Listitem',
  templateUrl: './listitem.component.html',
  styleUrls: ['./listitem.component.css']
})
export class ListitemComponent implements OnInit {
  @Input() data:any;

  price;
  id;
title;
stock;
  setred=false;



  constructor() { }

  ngOnInit() {
    if (this.data.stock<10) {
          this.setred = true;
         }

    this.title = this.data.title;
    this.id= this.data.id;
    this.price= this.data.price;
    this.stock= this.data.stock;
  }

}
